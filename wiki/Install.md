# Windows

On the [download page](http://code.google.com/p/jbrout/downloads/list), you will find full installer for Windows platforms

* * *

# GNU/LINUX

## Official packages

### Debian/Ubuntu

Add this repository to your sources.list : `deb http://jbrout.free.fr/download/debian binary/`

update your packages : `sudo apt-get update`

and install jbrout : `sudo apt-get install jbrout`

### Fedora

`yum install jbrout`

### Gentoo

Gentoo installation page

### RPM distributions

[RPM package](http://jbrout.free.fr/download/rpm/), (you should install the provided python2.4 lxml package too, but it seems it's broken on recent mandriva. Prefer [the sure way to install lxml](http://codespeak.net/lxml/installation.html) )!!

### Others

You should follow the process to get the svn version. Or get a [tar.gz version](http://jbrout.free.fr/download/sources/) of the application !

## Packages by others maintainers

### archlinux (thanx to Guital)

[jbrout-svn 0.2](http://aur.archlinux.org/packages.php?do_Details=1&ID=2150&O=0&L=0&C=0&K=jbrout&SB=n&SO=a&PP=25&do_MyPackages=0&do_Orphans=0) and [lxml0.7](http://aur.archlinux.org/packages.php?do_Details=1&ID=8693&O=0&L=0&C=0&K=jbrout&SB=n&SO=a&PP=25&do_MyPackages=0&do_Orphans=0&SeB=)


